<?php
namespace Library\AWS;
use \Library\AWS\Services\ServiceInterface;
class ServiceProvider
{
    protected $access_key;
    protected $secret_key;

    protected $service;

    public function __construct($access_key, $secret_key, $region = "ap-northeast-1")
    {
        $this->access_key = $access_key;
        $this->secret_key = $secret_key;
        $this->region     = $region;
    }

    public function getInstance(ServiceInterface $service)
    {
        return $service->getInstance($this->access_key, $this->secret_key, $this->region);
    }
}
