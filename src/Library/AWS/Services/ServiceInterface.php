<?php
namespace Library\AWS\Services;
interface ServiceInterface
{
    /**
     * for Common getInstanceInterfaceMethod
     * AWS\SerivceProvider::getInstace Depency Injected
     * @param String AWS_Access_Key
     * @param String AWS_Access_Secret_Key
     * @throws \Aws\Common\Exception
     * @return AWS-SDK-PHP classes
     */
    public function getInstance($access_key, $secret_key, $region = "ap-northeast-1");
}
