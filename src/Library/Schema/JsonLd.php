<?php
namespace Library\Schema;
use \Phalcon\Mvc\Model,
    \Phalcon\Mvc\Model\Resultset\Simple as Resultset,
    \stdClass;
/**
 * Phalcon用 JSON-LDスキーマ作成クラス
 * @depends \Phalcon\Mvc\Model \Phalcon\Mvc\Model\Resultset\Simple
 */
class JsonLd
{
    /**
     * create SinglePage Json-LD Format
     * @access public
     * @param \Phalcon\Mvc\Model $post
     * @return String JsonString
     */
    public static function createSingle(Model $post)
    {
        $object = new \stdClass;
        $object->{"@context"}  = 'http://schema.org';
        $object->{"@graph"}    = self::createPostSchema($post);
        return json_encode($object, JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT);
    }

    /**
     * create ListPage Json-LD Format
     * @access public
     * @param \Plancon\Mvc\Model\Resultset\Simple $post
     * @return String JsonString
     */
    public static function createList(Resultset $posts)
    {
        $object = new stdClass;
        $object->{"@context"}     = 'http://schema.org';
        $object->{"@type"}        = 'WebSite';
        $object->url              = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $object->potentialAction  = array(
            "@type"      => "SearchAction",
            "target"     => 'http://' . $_SERVER['HTTP_HOST']. "/search/?query={search_term}",
            "query-input"=> "required name=search_term"
        );
        foreach($posts as $post) {
            $object->{"@graph"}[] = self::createPostSchema($post);
        }
        return json_encode($object, JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT);
    }

    /**
     * スマホdeエロ画像ランド用記事スキーマ作成メソッド
     * @access private
     * @param \Phalcon\Mvc\Model | \Plancon\Mvc\Model\Resultset\Simple
     * @return stdClass
     */
    private static function createPostSchema($post)
    {
        $object = new stdClass;
        $object->{'@type'}     = 'Article';
        $object->name          = $post->title."【スマホdeエロ画像ランド】";
        $object->dataPublished = date("Y-m-d",strtotime($post->created));
        $object->image         = self::getFirstImage($post->imgs);
        $object->url           = 'http://' . $_SERVER['HTTP_HOST'] . "/posts/{$post->id}";
        $object->publisher     = (object)array(
            "@type"         => "Organization",
            "name"          => "スマホdeエロ画像ランド"
        );
        $object->interactionCount = array(
            "UserLike:{$post->star}"
        );
        return $object;
    }

    /**
     * 最初の画像URLを取得する
     * @access private
     * @param String $json
     * @return String
     */
    private static function getFirstImage($json)
    {
        $object = json_decode($json);
        return (isset($object[0])) ? $object[0] : null;
    }
}
