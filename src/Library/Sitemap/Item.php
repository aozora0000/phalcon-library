<?php
namespace Library\Sitemap;
class Item
{
    /**
     * @access protected
     * @var SimpleXMLElement
     */
    protected $item;

    /**
     * コンストラクタ
     * @param SimpleXMLElement $item
     */
    public function __construct(\SimpleXMLElement $item)
    {
        $this->item = $item;
    }


    /**
     * 各パラメータ用セッター
     * @param string $name
     * @param any    $value
     * @return void
     */
    public function __set($name,$value) {
        $this->item->addChild($name, $value);
    }

    /**
     * ゲッター
     * @return SimpleXMLElement
     */
    public function get()
    {
        return $this->item;
    }
}
