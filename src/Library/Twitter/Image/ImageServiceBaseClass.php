<?php
namespace Library\Twitter\Image;

class ImageServiceBaseClass
{
    /**
     * for Many ImageService Base classe
     * @final
     * @access public
     * @param  String $url
     * @return String $result
     * @throws \Exception
     */
    final public static function getImage($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,     $url );
        curl_setopt($ch, CURLOPT_HEADER,  false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        $result = curl_exec($ch);
        if($errno = curl_errno($ch)) {
            throw new \Exception("Curl error: {$errno}");
        }
        return $result;
    }
}
