<?php
namespace Library\Twitter\Image;

class ProfileImage extends ImageServiceBaseClass
{
    public static function check($url)
    {
        return (bool)strpos($url,"pbs.twimg.com/profile_images");
    }

    public static function get($url)
    {
        $url = self::getOriginalUrl($url);
        return self::getImage($url);
    }

    public static function getOriginalUrl($url)
    {
        return preg_replace("/\_(normal|large|small)/", "", $url);
    }
}
