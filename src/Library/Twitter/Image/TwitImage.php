<?php
namespace Library\Twitter\Image;

class TwitImage extends ImageServiceBaseClass
{
    public static function check($url)
    {
        return (bool)strpos($url,"pbs.twimg.com");
    }

    public static function get($url)
    {
        $url = self::getOriginalUrl($url);
        return self::getImage("$url:large");
    }

    public static function getOriginalUrl($url)
    {
        return preg_replace("/\:(large|small)$/", "", $url);
    }
}
