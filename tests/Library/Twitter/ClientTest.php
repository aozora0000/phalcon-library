<?php
use Library\Twitter\Client;

class ClientTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->client = new Client(
            $_ENV['TWITTER_ACCESS_KEY'],
            $_ENV['TWITTER_SECRET_KEY'],
            $_ENV['TWITTER_TOKEN_ACCESS_KEY'],
            $_ENV['TWITTER_TOKEN_SECRET_KEY']
        );
    }

    public function testGetTimeLineSuccessCase()
    {
        $timeline = $this->client->getTimeline(200,591164680770457600);
        $this->assertGreaterThan(1,count($timeline));
    }

    /**
     * @expectedException \Exception
     */
    public function testGetTimeLineFailedCase()
    {
        $timeline = $this->client->getTimeline(null,0);
        $this->fail("Not Call Exception",$json);
    }

    /**
     * @atension
     * 同一ユーザーでやりすぎるとユーザー追加が出来なくなるよー
     */
    public function testAddUser()
    {
        //$this->assertTrue($this->client->addUser("aozora0000"));
    }

    /**
     * @atension
     * 上のメソッドと同じく
     */
    public function testDelUser()
    {
        //$this->assertTrue($this->client->deleteUser("18421696"));
    }

    public function testGetUserID()
    {
        $user_id = $this->client->getUserID("aozora0000");
        $this->assertEquals(18421696,$user_id);
    }

    public function testGetUserName()
    {
        $screen_name = $this->client->getUserName(18421696);
        $this->assertEquals("aozora0000",$screen_name);
    }

    /**
     * @group now
     */
    public function testGetUserProfiles()
    {
        $increment = 0;
        $users = array(18421696,201411232,949438177,3871051);
        $results = $this->client->getUserProfiles($users);
        foreach($results as $result) {
            $this->assertEquals($users[$increment], $result->id);
            $increment++;
        }
    }

    public function  testGetFollows()
    {
        $follows = $this->client->getFollowsProfile("aozora0000");
        $ids = array_map(function($follow) { return $follow->id; }, $follows);
        $this->assertGreaterThan(1,$ids);
    }

    public function testIsFollow()
    {
        $result = $this->client->isFollow("jackfrost_bot");
        $this->assertTrue($result);
    }
}
