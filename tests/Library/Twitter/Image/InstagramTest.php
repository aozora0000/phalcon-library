<?php
use Library\Twitter\Image\Instagram;

class InstagramTest extends PHPUnit_Framework_TestCase
{
    CONST SUCCESS_IMAGE = "https://instagram.com/p/2AEdhmsRZs/";
    CONST FAILED_IMAGE  = "https://instagram.com/2AEdhmsRZs/";

    public function testCheckImageSuccessCase()
    {
        $this->assertTrue(Instagram::check(self::SUCCESS_IMAGE));
    }

    public function testCheckImageFailedCase()
    {
        $this->assertNotTrue(Instagram::check(self::FAILED_IMAGE));
    }

    public function testGetImageSuccessCase()
    {
        try {
            $image = Instagram::get(self::SUCCESS_IMAGE);
            $this->assertTrue(true);
        } catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     *
     */
    public function testGetImageFailedCase()
    {
        try {
            $image = Instagram::get(self::FAILED_IMAGE);
            $this->fail("Not Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testGetOriginalUrl()
    {
        $this->assertEquals("https://igcdn-photos-b-a.akamaihd.net/hphotos-ak-xfa1/t51.2885-15/11189387_490727804411681_1182269078_n.jpg", Instagram::getOriginalUrl(self::SUCCESS_IMAGE));
    }
}
